# aws_dynamodb_table
resource "aws_dynamodb_table" "Temperature" {
  name             = "Temperature"
  hash_key         = "id"
  read_capacity    = 1
  write_capacity   = 1

  attribute {
    name = "id"
    type = "S"
  }

}
